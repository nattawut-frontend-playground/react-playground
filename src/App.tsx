import React, { useState } from 'react';
import axios from 'axios';

function App() {

  //---------------------------------- DOWNLOAD ----------------------------------\\
  const handleDownload = () => {
    const downloadUrl = 'http://localhost:8080/download/servletresponse';
    window.open(downloadUrl, 'none');
  };
  //-------------------------------------------------------------------------------\\



  //---------------------------------- UPLOAD -------------------------------------\\
  const [file1, setFile1] = useState('');

  const handleFileChange1 = (event: any) => {
    setFile1(event.target.files[0]);
  };

  const handleUpload1 = async () => {
    const formData1 = new FormData();
    formData1.append('file', file1);

    try {
      await axios.post('http://localhost:8080/upload/file', formData1, {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      });
      alert('File uploaded successfully!');
    } catch (error) {
      console.error('Error uploading file:', error);
      alert('Error uploading file. Please try again.');
    }
  }
  //-------------------------------------------------------------------------------\\


  //---------------------------------- UPLOAD WITH DATA ----------------------------\\
  const [file2, setFile2] = useState('');
  const [formData2, setFormData2] = useState({
    id: '',
    name: ''
  });

  const handleFileChange2 = (event: any) => {
    setFile2(event.target.files[0]);
  };

  const handleFormDataChange2 = (event: any) => {
    const { name, value } = event.target;
    setFormData2({
      ...formData2,
      [name]: value
    });
  };

  const handleUpload2 = async () => {
    const formDataObj = new FormData();
    formDataObj.append('file', file2);
    formDataObj.append('data', JSON.stringify(formData2));

    try {
      await axios.post('http://localhost:8080/upload/file-data', formDataObj, {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      });
      alert('File uploaded successfully!');
    } catch (error) {
      console.error('Error uploading file:', error);
      alert('Error uploading file. Please try again.');
    }
  }
  //-----------------------------------------------------------------------------\\



  return (
    <div className="App">

      <h1>PDF Downloader</h1>
      <button onClick={handleDownload}>Download PDF</button>




      <h1>File Uploader</h1>
      <input type="file" onChange={handleFileChange1} />
      <button onClick={handleUpload1} disabled={!file1}>Upload</button>



      <h1>File Uploader with Request Body</h1>
      <div>
        <label>ID:</label>
        <input type="text" name="id" value={formData2.id} onChange={handleFormDataChange2} />
      </div>
      <div>
        <label>Name:</label>
        <input type="text" name="name" value={formData2.name} onChange={handleFormDataChange2} />
      </div>
      <div>
        <input type="file" onChange={handleFileChange2} />
      </div>
      <button onClick={handleUpload2} disabled={!file2 || !formData2.id || !formData2.name}>Upload</button>

    </div>
  );
}

export default App;
